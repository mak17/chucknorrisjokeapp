package com.mak.joke.jokeapp.services;

/**
 * Created by Marcin Mak Ławicki on 2019-04-01.
 */
public interface JokeService {

    String getJoke();
}
